
#include <common.h>
#include <cli.h>
#include <command.h>
#include <console.h>
#include <env.h>
#include <search.h>
#include <errno.h>
#include <malloc.h>
#include <mapmem.h>
#include <watchdog.h>
#include <linux/stddef.h>
#include <asm/byteorder.h>
#include <asm/io.h>

#include "sysdata.h"




static int do_sysdata( struct cmd_tbl *cmdtp, int flag, int argc,
			char * const argv[] )
{
    int ret = 0;

    if ( argc == 2 ) {

        if ( strcmp( argv[1], "version" ) == 0 ) {

            char *version = NULL;
            ret = get_sysdata_version( &version );
            if ( ret == 0 ) {
                if ( version )
                    printf( "SysData version: %s\n", version );
                else
                    printf( "Error on retrieving data.\n" );
            } else
                printf( "Error on retrieving data.\n" );

        } else  if ( strcmp( argv[1], "dev" ) == 0 ) {

            const char *device = get_sysdata_device_name( );
            if ( device ) {
                char device_id[32];
                printf( "%s\n", device );
                if ( strcmp( device, "MMC0" ) == 0 )
                    sprintf( device_id, "mmcblk0" );
                else if ( strcmp( device, "MMC1" ) == 0 )
                    sprintf( device_id, "mmcblk1" );
                else if ( strcmp( device, "MMC2" ) == 0 )
                    sprintf( device_id, "mmcblk2" );
                else
                    sprintf( device_id, "none" );

                env_set( "sysdata_device", device_id );
            } else
                printf( "Error on retrieving data.\n" );

        } else if ( strcmp( argv[1], "initialize" ) == 0 ) {    
            ret = sysdata_init( );
            if ( ret )
                printf( "Error (%d), table not initialized.\n", ret );
            else
                printf( "Table initialized.\n" );
        }

    } else if ( argc >= 3 ) {

        if ( strcmp( argv[1], "switchslot" ) == 0 ) {
            int               i;
            switching_table_t switch_data;

            if ( strcmp( argv[2], "status" ) == 0 ) {
                ret = get_switchslot_data( &switch_data );
                if ( ret )
                    printf( "Error (%d) on retrieving data.\n", ret );
                else {
                    for ( i = 1 ; i <= SYSDATA_MAX_SLOTS ; i++ ) {
                        printf( "Boot on slot %c: %05d\n", SLOT_NUM2ID(i), switch_data.boot_slot_counter[i-1] );
                        printf( "Consecutive fail boot on slot %c: %01d\n", SLOT_NUM2ID(i), switch_data.boot_fail_counter[i-1]);
                    }
                    printf( "---------------\n" );
                    printf( "Boot order: " );
                    for ( i = 1 ; i <= SYSDATA_MAX_SLOTS ; i++ ) {
                        printf( " %c ", SLOT_NUM2ID(switch_data.boot_slot_order[i-1]) );
                    }
                    printf( "\n---------------\n" );
                    printf( "Current boot slot: %c\n", SLOT_NUM2ID(switch_data.current_boot_slot) );
                    printf( "Previous boot slot: %c\n", SLOT_NUM2ID(switch_data.previous_boot_slot) );
                    printf( "Fail Boot counter state: %s\n", 
                                GET_FAIL_BOOT_COUNT_ENABLE(switch_data.enable_flags) ? "enabled" : "disabled" );
                    printf( "Global Boot counter state: %s\n",
                                GET_GLOBAL_BOOT_COUNT_ENABLE(switch_data.enable_flags) ? "enabled" : "disabled" );
                    
                }
            }
            
            if ( strcmp( argv[2], "global_counter" ) == 0 ) {
                if ( argc == 4 ) {
                    if ( strcmp( argv[3], "enable" ) == 0 ) {
                        if ( sysdata_switchslot_set_enable_global_counter( 1 ) == 0 )
                            printf( "Global Boot counter enabled.\n" );
                        else
                            printf( "Error: invalid operation.\n" );
                    } else if ( strcmp( argv[3], "disable" ) == 0 ) {
                        if ( sysdata_switchslot_set_enable_global_counter( 0 ) == 0 )
                            printf( "Global Boot counter disabled.\n" );
                        else
                            printf( "Error: invalid operation.\n" );
                    } else
                       printf( "Error: invalid parameters.\n" ); 
                }
            }

            if ( strcmp( argv[2], "fail_counter" ) == 0 ) {
                if ( argc == 4 ) {
                    if ( strcmp( argv[3], "enable" ) == 0 ) {
                        if ( sysdata_switchslot_set_enable_fail_counter( 1 ) == 0 )
                            printf( "Fail Boot counter enabled.\n" );
                        else
                            printf( "Error: invalid operation.\n" );
                    } else if ( strcmp( argv[3], "disable" ) == 0 ) {
                        if ( sysdata_switchslot_set_enable_fail_counter( 0 ) == 0 )
                            printf( "Fail Boot counter disabled.\n" );
                        else
                            printf( "Error: invalid operation.\n" );
                    } else
                       printf( "Error: invalid parameters.\n" ); 
                }
            }

            if ( strcmp( argv[2], "set_primary" ) == 0 ) {
                if ( argc == 4 ) {
                    char slot = (char)argv[3][0];
                    if ( SLOT_IS_VALID(SLOT_ID2NUM(slot)) ) {
                        if ( sysdata_switchslot_set_primary( slot ) == 0 )
                            printf( "Slot %c set as primary.\n", slot );
                        else
                            printf( "Error: invalid operation.\n" );
                    } else {
                        printf( "Error: %c is not a valid slot.\n", slot );
                    }
                }
            }

            if ( strcmp( argv[2], "remove_slot" ) == 0 ) {
                if ( argc == 4 ) {
                    char slot = (char)argv[3][0];
                    if ( SLOT_IS_VALID(SLOT_ID2NUM(slot)) ) {
                        if ( sysdata_switchslot_remote_from_boot_order( slot ) == 0 )
                            printf( "Slot %c has been removed from boot order list.\n", slot );
                        else
                            printf( "Error: invalid operation.\n" );
                    } else {
                        printf( "Error: %c is not a valid slot.\n", slot );
                    }
                }
            }

            if ( strcmp( argv[2], "invalidate_slot" ) == 0 ) {
                if ( argc == 4 ) {
                    char slot = (char)argv[3][0];
                    if ( SLOT_IS_VALID(SLOT_ID2NUM(slot)) ) {
                        if ( sysdata_switchslot_invalidate_slot( slot ) == 0 )
                            printf( "Slot %c has been invalidated.\n", slot );
                        else
                            printf( "Error: invalid operation.\n" );
                    } else {
                        printf( "Error: %c is not a valid slot.\n", slot );
                    }
                }
            }

        }
    } 
        
    return 4;
}


U_BOOT_CMD_COMPLETE(
	sysdata, CONFIG_SYS_MAXARGS, 1,	do_sysdata,
	"SECO system data manager",
    "sysdata [command] [options]\n"
    "   command:\n"
    "   - version - shows sysdata version.\n"
    "   - dev     - shows current device used to read/write sysdata.\n"
    "               (sets env sysdata_device with reference to kernel space device id).\n"
    "   - initialize - reset all memory reserved to SysData with factory data.\n"
    "   - switchslot <subcommand>\n"
    "        subcommand:\n"
    "        - status - shows current state switching slot system.\n"
    "        - global_counter [enable | disable] - enable/disable the global boot counter.\n"
    "        - fail_counter [enable | disable]   - enable/disable the fail boot counter.\n"
    "        - set_primary [slot ID]             - set a slot as primary in boot order list.\n"
    "        - remove_slot [slot ID]             - remove a slot from boot order list.\n"
    "        - invalidate_slot [slot ID]         - invalidate a slot.\n",
	var_complete
);
