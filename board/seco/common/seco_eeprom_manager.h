/*****************************************************************************

File Name  : seco_eeprom_manager.h
Description: Seco eeprom management library header

Copyright (C) 2024 Seco SpA
Author: shirin raeisi <shirin.raeisi@seco.com>

*****************************************************************************/
#ifndef __SECO_EEPROM_MANAGER_H
#define __SECO_EEPROM_MANAGER_H

#include <net.h>

#define EEPROM_OFFSET_VERSION   0
#define EEPROM_OFFSET_CRC       2
#define EEPROM_OFFSET_DATA      6
#define EEPROM_SIZE				256

#define MAX_MSG_LENGTH			16

#define SSECT_HEADER_LEN        2		// UID + LEN
#define SSECT_DATA_MAX_LEN      32

#define VERSION_MAJOR           0
#define VERSION_MINOR           2

enum
{
	UID_BOARD_SERIAL_NUMBER     = 0x00,
	UID_BOARD_PART_NUMBER       = 0x01,
	UID_SYSTEM_SERIAL_NUMBER    = 0x02,
	UID_SYSTEM_PART_NUMBER      = 0x03,
	UID_MAC_ADDRESS             = 0x04,
	UID_PANEL_ID                = 0x05,
	UID_PANEL_ORIENTATION       = 0x06,
	UID_NONE                    = 0xff
};

struct eeprom_ssect
{
	u8 uid;
	u8 len;
	u8 data[SSECT_DATA_MAX_LEN+1];
};

struct eeprom_data
{
	u8 version[2];
	u32 crc;
	u8 mac_address[ARP_HLEN];
	u8 panel_id;
	u8 orientation;
};

#define LANDSCAPE               0x4c	// L
#define REVERSE_PORTRAIT        0x4e	// N
#define PORTRAIT                0x50	// P
#define REVERSE_LANDSCAPE       0x52	// R

#ifdef CONFIG_SECO_EEPROM_MANAGER_CUSTOM_UIDS
#define is_custom_uid(x)        (x > 0xEF) && (x < 0xFF)
extern void populate_custom_uid(struct eeprom_ssect subsection);
#endif

// Exported functions
// ---------------------
int seco_eeprom_init(u8 i2c_bus_number, u8 i2c_addr);
int seco_eeprom_get_macaddr(u8 *mac_addr);
int seco_eeprom_get_panel_id(u8 *panel);
int seco_eeprom_get_orientation(u8 *orientation);
int seco_eeprom_print_all(void);
#endif /* __SECO_EEPROM_MANAGER_H */
