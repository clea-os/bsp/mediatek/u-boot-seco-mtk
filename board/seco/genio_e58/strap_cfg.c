/*
 *
 * Reading E58 Board type
 *
 * oleksii.kutuzov@seco.com
 *
 */

#include <common.h>
#include <env.h>
#include <log.h>
#include <dm.h>
#include <i2c.h>
#include <env.h>
#include <stdio.h>
#include <string.h>
#include "strap_cfg.h"

#define BUS_NUM 3
#define CHIP_ADDR 0x40
#define CHIP_ADDR_DUMMY 0x41
#define REG_SECO_CODE0 0x80
#define REG_SECO_CODE1 0x81
#define RAM_CODE_REG 0x90
#define FW_VERSION 0xf0

static uint8_t seco_code_value[2];
static uint8_t seco_code_cached = 0;
static uint8_t ram_code_value;
static uint8_t ram_code_cached = 0;

/* ____________________________________________________________________________
  |                                                                            |
  |                                 E58 STRAPS                                 |
  |____________________________________________________________________________|
*/

static int read_stm32_register(int reg_addr, uint8_t *data)
{

	int ret;
	struct udevice *i2c_dev;
	struct udevice *i2c_dev_dummy;

	ret = i2c_get_chip_for_busnum(BUS_NUM, CHIP_ADDR, 1, &i2c_dev);
	if (ret)
	{
		printf("\nError getting Embedded Controller!\n");
		return 1;
	}

	// I2C on Genio gets stuck if only probed without read/write afterwards
	// As a workaroud probe some other address before the second query
	i2c_get_chip_for_busnum(BUS_NUM, CHIP_ADDR_DUMMY, 1, &i2c_dev_dummy);

	ret = dm_i2c_read(i2c_dev, reg_addr, data, 1);
	if (ret)
	{
		printf("\nError reading from Embedded Controller!\n");
		return 1;
	}

	debug("ret: 0x%02x\n", *data);

	return 0;
}

static int strap_get_seco_cfg(int num)
{
	/*
	SECO_CODE

	|....|....|                                                         Display  (11......= undef,  00......= Opt eDP1, No LVDS, 10......= Opt Dual-LVDS, No eDP1, 01......= Opt Single-LVDS, eDP1)
	|    |    |....|                                                    USB      (..0.....= no USB, ..1.....= with USB)
	|    |    |    |....|....|....|                                     Version  (...000..= .... REVA, REVB, REVCX ....)
	|    |    |    |    |    |    |....|                                CAN      (......0.= no CAN, ......1.= with CAN)
	|    |    |    |    |    |    |    |....|                           Option Commercial Temperaturew Range (not used a.t.m.)
	|    |    |    |    |    |    |    |    |....|                      SER2     (.......0= with SER2, .......1= no SER2)
	|    |    |    |    |    |    |    |    |    |.....|                Not yet defined
	|    |    |    |    |    |    |    |    |    |     |.....|          Not yet defined
	|    |    |    |    |    |    |    |    |    |     |     |.....|    Not yet defined
	|Bit1|Bit2|Bit3|Bit4|Bit5|Bit6|Bit7|Bit8|Bit9|Bit10|Bit11|Bit12|

	i.e.
	SOM-SMARC-Genio700 REVA0   RE58-1AA2-1611-E0        01100010          | Opt Single-LVDS, eDP1  |  USB   | REVA0 |  CAN   |
							   RE58-1AA1-2611-E0        10100010          | Opt Dual-LVDS, No eDP1 |  USB   | REVA0 |  CAN   |
							   RE58-1AA0-0100-C0        00000000          | Opt eDP1, No LVDS      | no USB | REVA0 | No CAN |
	SOM-SMARC-Genio700 REVB    EE58-3EA2-3621-I2        101100100111      | Opt Dual-LVDS, No eDP1 |  USB   | REVB1 |  CAN   | SER2 |
	*/

	int ret;

	if (seco_code_cached == 1)
	{
		return seco_code_value[num];
	}

	ret = read_stm32_register(REG_SECO_CODE0, &seco_code_value[0]);
	if (ret)
	{
		printf("\nError reading SECO_CODE0!\n");
		return 1;
	}

	ret = read_stm32_register(REG_SECO_CODE1, &seco_code_value[1]);
	if (ret)
	{
		printf("\nError reading SECO_CODE1!\n");
		return 1;
	}

	debug("seco_code0_hex: %x\n", seco_code_value[0]);
	debug("seco_code1_hex: %x\n", seco_code_value[1]);

#ifdef DEBUG
	uint8_t seco_code[12];
	seco_code[BIT_1] = (seco_code_value[0] & 0x01);
	seco_code[BIT_2] = (seco_code_value[0] & 0x02) >> 1;
	seco_code[BIT_3] = (seco_code_value[0] & 0x04) >> 2;
	seco_code[BIT_4] = (seco_code_value[0] & 0x08) >> 3;
	seco_code[BIT_5] = (seco_code_value[0] & 0x10) >> 4;
	seco_code[BIT_6] = (seco_code_value[0] & 0x20) >> 5;
	seco_code[BIT_7] = (seco_code_value[0] & 0x40) >> 6;
	seco_code[BIT_8] = (seco_code_value[0] & 0x80) >> 7;
	seco_code[BIT_9] = (seco_code_value[1] & 0x01);
	seco_code[BIT_10] = (seco_code_value[1] & 0x02) >> 1;
	seco_code[BIT_11] = (seco_code_value[1] & 0x04) >> 2;
	seco_code[BIT_12] = (seco_code_value[1] & 0x08) >> 3;

	char binary_str[9];
	sprintf(binary_str, "%d%d%d%d%d%d%d%d%d%d%d%d",
			seco_code[BIT_1], seco_code[BIT_2],
			seco_code[BIT_3], seco_code[BIT_4],
			seco_code[BIT_5], seco_code[BIT_6],
			seco_code[BIT_7], seco_code[BIT_8],
			seco_code[BIT_9], seco_code[BIT_10],
			seco_code[BIT_11], seco_code[BIT_12]);
	debug("Binary: %s\n", binary_str);
#endif

	seco_code_cached = 1;
	return seco_code_value[num];
}

int strap_get_ram_cfg(void)
{
	int ret;

	if (ram_code_cached == 1)
	{
		return ram_code_value;
	}

	ret = read_stm32_register(RAM_CODE_REG, &ram_code_value);
	if (ret)
	{
		printf("\nError reading RAM_CODE!\n");
		return 1;
	}

	debug("RAM code: %x\n", ram_code_value);

	ram_code_cached = 1;
	return ram_code_value;
};

int strap_get_revision_cfg(void)
{
	return ((strap_get_seco_cfg(0) & 0x38) >> 3);
};

int strap_get_display_cfg(void)
{
	return ((strap_get_seco_cfg(0) & 0x01) << 1) |
		   ((strap_get_seco_cfg(0) & 0x02) >> 1);
};

int strap_get_usb_cfg(void)
{
	return (strap_get_seco_cfg(0) & 0x04) >> 2;
};

int strap_get_can_cfg(void)
{
	return (strap_get_seco_cfg(0) & 0x40) >> 6;
};

int strap_get_ser2_cfg(void)
{
	return (strap_get_seco_cfg(1) & 0x01);
};

void strap_show(void)
{
	strap_conf_t e58_strap_conf;
	e58_strap_conf.ram_size = GET_E58_RAM_STRAPS;
	e58_strap_conf.hw_rev = GET_E58_REV_STRAPS;
	e58_strap_conf.disp = GET_E58_DISP_STRAPS;
	e58_strap_conf.usb = GET_E58_USB_STRAPS;
	e58_strap_conf.can = GET_E58_CAN_STRAPS;
	e58_strap_conf.ser2 = GET_E58_SER2_STRAPS;

	printf("Straps:\n");

	printf(" - RAM code: %d (", e58_strap_conf.ram_size);
	switch (e58_strap_conf.ram_size)
	{
	case RAM_1GB:
		printf("1GB");
		break;
	case RAM_2GB:
		printf("2GB");
		break;
	case RAM_4GB:
		printf("4GB");
		break;
	case RAM_8GB:
		printf("8GB");
		break;
	case RAM_16GB:
		printf("16GB");
		break;
	default:
		printf("Unknown");
		break;
	}
	printf(")\n");

	printf(" - Revision code: %d (", e58_strap_conf.hw_rev);
	switch (e58_strap_conf.hw_rev)
	{
	case REV_A0:
		printf("REVA");
		break;
	case REV_B1:
		printf("REVB");
		break;
	default:
		printf("Unknown");
		break;
	}
	printf(")\n");

	printf(" - Display code: %d (", e58_strap_conf.disp);
	switch (e58_strap_conf.disp)
	{
	case DISP_CFG_D0:
		printf("Opt eDP1, No LVDS");
		break;
	case DISP_CFG_D2:
		printf("Opt Single-LVDS, eDP1");
		break;
	case DISP_CFG_D1:
		printf("Opt Dual-LVDS, No eDP1");
		break;
	case DISP_CFG_UNDEF:
		printf("Undefined");
		break;
	default:
		printf("Unknown");
		break;
	}
	printf(")\n");

	printf(" - USB code: %d (", e58_strap_conf.usb);
	switch (e58_strap_conf.usb)
	{
	case YES_USB:
		printf("USB HUB");
		break;
	case NO_USB:
		printf("NO USB HUB");
		break;
	default:
		printf("Unknown");
		break;
	}
	printf(")\n");

	printf(" - CAN code: %d (", e58_strap_conf.can);
	switch (e58_strap_conf.can)
	{
	case YES_CAN:
		printf("CAN");
		break;
	case NO_CAN:
		printf("NO CAN");
		break;
	default:
		printf("Unknown");
		break;
	}
	printf(")\n");

	printf(" - SER2 code: %d (", e58_strap_conf.ser2);
	switch (e58_strap_conf.ser2)
	{
	case YES_SER2:
		printf("SER2");
		break;
	case NO_SER2:
		printf("NO SER2");
		break;
	default:
		printf("Unknown");
		break;
	}
	printf(")\n");
}

void strap_dtbo_ram_setup(void)
{
	const char *ram_conf_val = "";

	// Get the current RAM configuration
	unsigned int ram_cfg = GET_E58_RAM_STRAPS;

	// Determine the appropriate config string based on RAM configuration
	switch (ram_cfg)
	{
	case RAM_1GB:
		ram_conf_val = "#conf-dram-1gb.dtbo";
		break;
	case RAM_2GB:
		ram_conf_val = "#conf-dram-2gb.dtbo";
		break;
	case RAM_4GB:
		ram_conf_val = "#conf-dram-4gb.dtbo";
		break;
	case RAM_8GB:
		ram_conf_val = "#conf-dram-8gb.dtbo";
		break;
	case RAM_16GB:
		ram_conf_val = "#conf-dram-16gb.dtbo";
		break;
	default:
		ram_conf_val = "";
		break;
	}

	// Set the ram_conf environment variable
	if (env_set("ram_conf", ram_conf_val))
	{
		printf("Error: Failed to set environment variable 'ram_conf'\n");
		return;
	}

	debug("Set 'ram_conf' to '%s'\n", ram_conf_val);
}

void strap_dtbo_can_setup(void)
{
	const char *can_conf_val = "";

	if (E58_HAS_CAN)
		can_conf_val = "#conf-peripheral-can.dtbo";

	// Set the ram_conf environment variable
	if (env_set("can_conf", can_conf_val))
	{
		printf("Error: Failed to set environment variable 'can_conf'\n");
		return;
	}

	debug("Set 'can_conf' to '%s'\n", can_conf_val);
}

void strap_dtbo_ser2_setup(void)
{
	const char *ser2_conf_val = "";

	if (E58_HAS_NO_SER2)
		ser2_conf_val = "#conf-peripheral-disable-ser2.dtbo";

	// Set the ram_conf environment variable
	if (env_set("ser2_conf", ser2_conf_val))
	{
		printf("Error: Failed to set environment variable 'ser2_conf'\n");
		return;
	}

	debug("Set 'ser2_conf' to '%s'\n", ser2_conf_val);
}
