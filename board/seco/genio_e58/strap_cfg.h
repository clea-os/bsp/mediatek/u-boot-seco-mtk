#ifndef _E58_REVISION_H_
#define _E58_REVISION_H_

int strap_get_ram_cfg(void);
int strap_get_revision_cfg(void);
int strap_get_display_cfg(void);
int strap_get_usb_cfg(void);
int strap_get_can_cfg(void);
int strap_get_ser2_cfg(void);

void strap_show(void);
void exp_strap_show(void);

void strap_dtbo_ram_setup(void);
void strap_dtbo_can_setup(void);
void strap_dtbo_ser2_setup(void);

typedef enum
{
	BIT_1,	// Display Config Bit 1
	BIT_2,	// Display Config Bit 2
	BIT_3,	// USB Config Bit
	BIT_4,	// PCB Revision Bit 1
	BIT_5,	// PCB Revision Bit 2
	BIT_6,	// PCB Revision Bit 3
	BIT_7,	// CAN Config Bit
	BIT_8,	// Temperature Range bit (not used a.t.m.)
	BIT_9,	// SER2 Config Bit
	BIT_10, // Not yet defined
	BIT_11, // Not yet defined
	BIT_12, // Not yet defined
} SECO_CODE_BITS;

typedef enum
{
	REV_A0 = 0x0,
	REV_B1 = 0x1,
} BOARD_REV;

typedef enum
{
	RAM_1GB = 0x1,
	RAM_2GB = 0x2,
	RAM_4GB = 0x4,
	RAM_8GB = 0x8,
	RAM_16GB = 0xC,
} RAM_STRAPS;

typedef enum
{
	DISP_CFG_D0 = 0x0,	  // Opt eDP1, No LVDS
	DISP_CFG_D2 = 0x1,	  // Opt Single-LVDS, eDP1
	DISP_CFG_D1 = 0x2,	  // Opt Dual-LVDS, No eDP1
	DISP_CFG_UNDEF = 0x3, // Undefined
} DISPLAY_STRAPS;

typedef enum
{
	NO_USB = 0x0,  // Without USB-Hub
	YES_USB = 0x1, // With USB-Hub
} USB_STRAPS;

typedef enum
{
	NO_CAN = 0x0,  // Without CAN
	YES_CAN = 0x1, // With CAN
} CAN_STRAPS;

typedef enum
{
	YES_SER2 = 0x0, // With SER2
	NO_SER2 = 0x1,	// Without SER2
} SER2_STRAPS;

typedef struct
{
	uint8_t ram_size;
	uint8_t hw_rev;
	uint8_t disp;
	uint8_t usb;
	uint8_t can;
	uint8_t ser2;
} strap_conf_t;

#define GET_E58_RAM_STRAPS (strap_get_ram_cfg())
#define E58_IS_1GB (GET_E58_RAM_STRAPS == RAM_1GB)
#define E58_IS_2GB (GET_E58_RAM_STRAPS == RAM_2GB)
#define E58_IS_4GB (GET_E58_RAM_STRAPS == RAM_4GB)
#define E58_IS_8GB (GET_E58_RAM_STRAPS == RAM_8GB)
#define E58_IS_16GB (GET_E58_RAM_STRAPS == RAM_16GB)

#define GET_E58_REV_STRAPS (strap_get_revision_cfg())
#define E58_IS_REVA0 (GET_E58_REV_STRAPS == REV_A0)
#define E58_IS_REVB1 (GET_E58_REV_STRAPS == REV_B1)

#define GET_E58_DISP_STRAPS (strap_get_display_cfg())
#define E58_DISP_IS_D0 (GET_E58_DISP_STRAPS == DISP_CFG_D0)
#define E58_DISP_IS_D2 (GET_E58_DISP_STRAPS == DISP_CFG_D2)
#define E58_DISP_IS_D1 (GET_E58_DISP_STRAPS == DISP_CFG_D1)
#define E58_DISP_IS_UNDEF (GET_E58_DISP_STRAPS == DISP_CFG_UNDEF)

#define GET_E58_USB_STRAPS (strap_get_usb_cfg())
#define E58_HAS_USB (GET_E58_USB_STRAPS == YES_USB)

#define GET_E58_CAN_STRAPS (strap_get_can_cfg())
#define E58_HAS_CAN (GET_E58_CAN_STRAPS == YES_CAN)

#define GET_E58_SER2_STRAPS (strap_get_ser2_cfg())
#define E58_HAS_NO_SER2 (GET_E58_SER2_STRAPS == NO_SER2)

#endif
