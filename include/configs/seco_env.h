#ifndef __SECO_CONFIG_H
#define __SECO_CONFIG_H

#include <linux/stringify.h>

#define MACRO_ENV_KERNEL_SRC_USDHCI \
    mmc dev ${kernel_device_id}; part start mmc ${kernel_device_id} ${kernel_partition} kernel_part_addr; part size mmc ${kernel_device_id} ${kernel_partition} kernel_part_size; mmc read ${kernel_addr_r} ${kernel_part_addr} ${kernel_part_size}

#define MACRO_ENV_RAMFS_SRC_USDHCI  \
    fatload mmc ${ramfs_device_id}:${ramfs_partition} ${ramdisk_addr_r} ${ramfs_file}; setenv ramfs "${ramdisk_addr_r}:${filesize}"

#define MACRO_ENV_OVERLAYS_CONFIGURE \
    setenv boot_conf "${boot_conf}${ram_conf}${can_conf}${ser2_conf}"

#define MACRO_ENV_BOOTARGS_BASE  \
    setenv bootargs " ${bootargs} root=PARTLABEL=${root_partition} rootwait"

#define ENV_MMCAUTODETECT "yes"

#define ENV_KERNEL_PARTITION "kernel"

#define ENV_ROOT_PARTITION "rootfs"

#define SECO_ENV \
	"mmcautodetect="ENV_MMCAUTODETECT"\0"                             \
    "kernel_partition="ENV_KERNEL_PARTITION"\0"                       \
    "root_partition="ENV_ROOT_PARTITION"\0"                           \
    "kernel_load2ram="__stringify(MACRO_ENV_KERNEL_SRC_USDHCI)"\0"    \
	"ramfs_load2ram="__stringify(MACRO_ENV_RAMFS_SRC_USDHCI)"\0"      \
    "dtbo_configure="__stringify(MACRO_ENV_OVERLAYS_CONFIGURE)"\0"    \
    "bootargs_base="__stringify(MACRO_ENV_BOOTARGS_BASE)"\0"    

// Override CONFIG_EXTRA_ENV_SETTINGS from processor header file
#undef CONFIG_EXTRA_ENV_SETTINGS
#define CONFIG_EXTRA_ENV_SETTINGS \
	"scriptaddr=0x40000000\0" \
	"fdt_addr_r=0x44000000\0" \
	"fdtoverlay_addr_r=0x44c00000\0" \
	"kernel_addr_r=0x45000000\0" \
	"ramdisk_addr_r=0x46000000\0" \
	"fdtfile=" CONFIG_DEFAULT_DEVICE_TREE ".dtb\0" \
	"splashimage=" __stringify(CONFIG_SYS_LOAD_ADDR) "\0" \
	"splashsource=mmc_fs\0" \
	"splashfile=logo.bmp\0" \
	"splashdevpart=0#bootassets\0" \
	"splashpos=m,m\0" \
    SECO_ENV \
	BOOTENV

#endif